from connection import engine
from os import environ
import pytest
import pandas as pd
from sqlalchemy.engine import Engine

@pytest.fixture
@pytest.mark.db
def conn():
    result = engine(environ.get("DB_CONN", "db_user:db_pass@localhost/db_name"))
    return result

def test_answer(conn: Engine):
    df = pd.read_sql('select 1, now()', con=conn)
    assert len(df.columns) == 2
