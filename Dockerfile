FROM python

COPY . .

RUN pip install pipenv \
 && pipenv install --deploy --system \
 && pip cache purge

CMD python src/app.py