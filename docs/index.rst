
Fizz Buzz
===============================

Fizz buzz is a group word game for children to teach them about division.
Players take turns to count incrementally, replacing any number divisible by three with the word "fizz", 
and any number divisible by five with the word "buzz".

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rules/game
   rules/quickstart

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

