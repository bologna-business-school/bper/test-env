Programming
===========

Fizz buzz (often spelled FizzBuzz in this context) has been used as an interview screening device for computer programmers.

Writing a program to output the first 100 FizzBuzz numbers is a relatively trivial problem requiring little more than a loop and conditional statements. 

However, its value in coding interviews is to analyze fundamental coding habits that may be indicative of overall coding ingenuity.

Prova su funzione
-----------------

.. autofunction:: app.connect

Prova su modulo
---------------

.. automodule:: app
   :members: