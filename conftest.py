# https://stackoverflow.com/a/58493116/3120219
# Automatically mark as db all tests with fixute conn
def pytest_collection_modifyitems(items):
    for item in items:
        if 'conn' in getattr(item, 'fixturenames', ()):
            item.add_marker("db")