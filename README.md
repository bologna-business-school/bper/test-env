
# Test Env

Questo è il progetto per verificare il funzionamento dell'ambiente di sviluppo.

Può essere eseguito:
- localmente
- via dev container

## Proxy

Il proxy deve essere configurato in due componenti:

### Docker client

Per impostare il proxy nel docker client impostare in questo file le valorizzazioni `~/.docker/config.json` come indicato in documentazione ufficiale: https://docs.docker.com/network/proxy/

### Docker build and container

Per impostare il proxy durante le fasi di build e di run possono essere impostate in due modalità:

- via variabile di ambiente (della macchina host)
- via file di configurazione personale (non committato)

Per verificare che vengano correttamente iniettate le variabili da macchina host si può verificare con il seguente comando:

```
$ cd .devcontainer
$ docker compose config
```

Per impostare le variabili sul file di configurazione personale è necessario inserirle nel file .env che deve apparire come segue:

``` bash
$ cat .devcontainer/.env

HTTP_PROXY=http://proxy.internal:8080
HTTPS_PROXY=http://proxy.internal:8080
NO_PROXY=git.internal
```

## Troubleshooting

Le informazioni necessarie per fare un troubleshooting sono:

```
cat ~/.docker/config.json
cat .devcontainer/.env
cat docker-compose.yml
```