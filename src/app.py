from os import environ
from sqlalchemy.engine import Engine, create_engine, BaseRow
from .calc import inc

def COnnect() -> Engine:
  conn_string = environ.get("DB_CONN")
  assert conn_string is not None, "Evironment variable DB_CONN is required"
  inc(2)
  return create_engine(f"postgresql+pg8000://{conn_string}", client_encoding='utf8')


if __name__ == '__main__':
  COnnect()
