from os import environ
import sqlalchemy as sa
from sqlalchemy.engine import Engine

def engine(conn_string: str) -> Engine:
    return sa.create_engine(f"postgresql+pg8000://{conn_string}", client_encoding='utf8')

def connect():
    conn_string = environ.get("DB_CONN")
    if conn_string == None:
        print("Evironment variable DB_CONN is required")
        exit(1)
    return engine(conn_string)
